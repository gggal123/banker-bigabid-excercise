from flask import Flask
from flask_socketio import SocketIO, emit
from banker.resources.services import amounts_wrapper as aw
from banker.dal.redis_helpers import map_auction_id, amount_and_campaign_by, clear_auction
from threading import Thread

app = Flask(__name__)
socketio = SocketIO(app)
amounts_wrapper = aw()


@socketio.on('can_offer')
def can_offer(json):
    """
    Checks if there's enough money for a campaign. If there is, deducts the amount from the current amount.
    On the background, maps auction_id: campaign id+requested_amount which will be used later by other routes.
    :param json: json with params: campaign_id, amount, auction_id
    :return: bool, Whether an offer can be served to the exchange by the Bidder.
    """
    requested_amount = json.get('amount')
    campaign_id = json.get('campaign_id')
    auction_id = json.get('auction_id')
    is_enough_money = amounts_wrapper.request_money(campaign_id, requested_amount)
    Thread(target=map_auction_id, args=(auction_id, campaign_id, requested_amount)).start()
    return is_enough_money


@socketio.on('auction_ended')
def auction_ended(json):
    """
    If an auction was lost, returns the deducted money back to the campaign balance.
    Anyway removes the auction from auction_id: campaign_id+amount mapping.
    :param json: json with params: auction_id, did_win (bool)
    """
    auction_id = json.get('auction_id')
    did_win = bool(json.get('did_win'))
    if not did_win:
        pass
        # Find the wasted amount and return it to the campaign (remotely)
        try:
            campaign_id, amount = amount_and_campaign_by(auction_id)
        except ValueError:
            return
        amounts_wrapper.refund(campaign_id, amount)
    # Remove auction from auction: campaign_id+amount mapping.
    clear_auction(auction_id)


if __name__ == '__main__':
    # We'll use socketio in order to achieve minimum latency between the bidder and the banker.
    # No need to establish a TCP handshake on each call.
    print('Listening on port 5000')
    socketio.run(app)
