import unittest
from banker.model.amounts_wrapper import AmountsWrapper
from banker.resources.services import redis


class TestAmountsWrapper(unittest.TestCase):
    __amounts_wrapper = None

    def setUp(self) -> None:
        # Should use a redis mock
        self.amounts_wrapper = AmountsWrapper(redis())

    def test_request_money(self):
        self.assertIsInstance(self.amounts_wrapper.request_money('blah', 1), bool)

    def test__request_more_if_needed(self):
        pass
