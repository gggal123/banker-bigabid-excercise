import unittest
from api import can_offer, auction_ended
import undecorated


class TestAPI(unittest.TestCase):
    def test_can_offer(self):
        global can_offer
        json = {'auction_id': "auction_id1", 'amount': 0.1, 'campaign_id': "campaign_id1"}
        can_offer = undecorated.undecorated(can_offer)
        self.assertIsInstance(can_offer(json), bool)

    def test_auction_ended(self):
        global auction_ended
        json = {'auction_id': "auction_id1", 'did_win': 0}
        auction_ended = undecorated.undecorated(auction_ended)
        auction_ended(json)
        json['did_win'] = 1
