from threading import Lock, Thread
from banker.resources.conf import AMOUNT_CHUNK, MIN_AMOUNT_TO_REQUEST_MORE
import struct


class AmountsWrapper:
    __amounts = {}
    __request_more_locks = {}

    def __init__(self, redis_client):
        self.redis = redis_client

    def request_money(self, campaign_id, requested_amount) -> bool:
        """
        Requests money from the Banker (local + remote) and finds out whether the amount is available.
        :param requested_amount: The amount needed by the bidder
        :param campaign_id:
        :return: True if amount is available, False, otherwise.
        """
        current_amount = self.__amounts.get(campaign_id)
        if not current_amount:
            self.__request_more_if_needed(campaign_id, requested_amount)
            current_amount = self.__amounts.get(campaign_id)
        else:
            Thread(target=self.__request_more_if_needed, args=(campaign_id, requested_amount)).start()
        if current_amount < requested_amount:
            return False
        self.__amounts[campaign_id] -= requested_amount
        return True

    def refund(self, campaign_id, amount):
        """
        Tries to make a refund to the local amount. If local amount is almost empty (or doesn't exist) refunds
        the redis. This function is quiet important and is atomicity should be discussed and improved (Maybe using
        a queue, not refunding the local cache etc.)
        :param campaign_id: campaign to refund
        :param amount: amount to refund
        """
        current_amount = self.__amounts.get(campaign_id)
        if current_amount:
            if current_amount > MIN_AMOUNT_TO_REQUEST_MORE:
                current_amount[campaign_id] += amount
                return
        # If local refund failed, try remote refund.
        self.__remote_refund(campaign_id, amount)

    def __request_more_if_needed(self, campaign_id, amount):
        """
        If the amount of money for the campaign is short, asks for more.
        (The idea is to avoid cases where the asking for more stalls the can_offer route. Money should always
        be available unless there no more. The thresholds for when to ask for more money can be discussed
        or modified on the run.
        :param campaign_id: campaign ic to ask for more money.
        :param amount: Amount needed.
        :return:
        """
        if self.campaign_lock(campaign_id).locked():
            # More money is currently being requested, no need to ask again.
            return
        with self.campaign_lock(campaign_id):
            # Ask for more money if there's not enough. Enough is the requested amount times 10.
            local_amount = self.__amounts.get(campaign_id)
            if not local_amount:
                self.__request_more(campaign_id)
                self.__amounts[campaign_id] = AMOUNT_CHUNK
            else:
                if local_amount < amount * 10 or local_amount < MIN_AMOUNT_TO_REQUEST_MORE:
                    self.__request_more(campaign_id)
                    self.__amounts[campaign_id] += AMOUNT_CHUNK

    def __request_more(self, campaign_id):
        """
        Asks for more money from redis. ***Ideally need to be done by a redis lua script.
        Concurrency will cause race conditions.***

        :param campaign_id: campaign id to ask more money for.
        :return: True if money was asked successfully, False otherwise.

        """
        currently_remote = float(self.redis.get(campaign_id).decode('utf-8'))
        if not currently_remote or currently_remote - AMOUNT_CHUNK < 0:
            return False
        currently_remote -= AMOUNT_CHUNK
        self.redis.mset({campaign_id: currently_remote})
        return True

    def __remote_refund(self, campaign_id, amount):
        """
        Makes a refund to the redis (typically after an auction loss).

        ***Ideally need to be done by a redis lua script.
        Concurrency will cause race conditions.***
        """
        with self.campaign_lock(campaign_id):
            currently_remote = float(self.redis.get(campaign_id).decode('utf-8'))
            if not currently_remote:
                self.redis.set(campaign_id, amount)
                return
            currently_remote += amount
            self.redis.mset({campaign_id: currently_remote})

    def campaign_lock(self, campaign_id) -> Lock:
        """
        Returns the campaign id "more money asking" lock. If lock doesn't exists, creates one.
        """
        if self.__request_more_locks.get(campaign_id):
            return self.__request_more_locks.get(campaign_id)
        self.__request_more_locks[campaign_id] = Lock()
        return self.__request_more_locks[campaign_id]
