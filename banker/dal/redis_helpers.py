from banker.resources.services import redis as red
from banker.resources.conf import REDIS_CONCAT_SYMBOL

redis = red()


def map_auction_id(auction_id, campaign_id, amount):
    """
    Maps auction id to campaign_id+str(amount) for use on the auction_ended route
    """
    redis.mset({auction_id: REDIS_CONCAT_SYMBOL.join([campaign_id, str(amount)])})


def amount_and_campaign_by(auction_id):
    """
    Retrieves remotely stored amount and campaign id by auction id.
    :return: packed campaign_id, amount
    """
    camp_and_amount = redis.get(auction_id)
    if not camp_and_amount:
        raise ValueError('auction_id wasnt found on remote')
    camp_and_amount = camp_and_amount.decode("utf-8")
    camp_and_amount = camp_and_amount.split(REDIS_CONCAT_SYMBOL)
    campaign_id = camp_and_amount[0]
    amount = float(camp_and_amount[1])
    return campaign_id, amount


def clear_auction(auction_id):
    """
    Removes auction id from redis, basically after auction has ended.
    """
    redis.delete(auction_id)
