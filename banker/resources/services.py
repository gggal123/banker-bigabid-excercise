from banker.resources.conf import *
import redis as red
from banker.model.amounts_wrapper import AmountsWrapper

__redis_client = None
__amounts_wrapper = None


def redis():
    """
    Initializes the Redis client, once. Can return a mock if needed.
    """
    global __redis_client
    if not __redis_client:
        __redis_client = red.Redis(host=REDIS_HOST, port=REDIS_PORT)
    return __redis_client


def amounts_wrapper() -> AmountsWrapper:
    """
    Initializes an AmountsWrapper instance, once. Can return a mock if needed.
    """
    global __amounts_wrapper
    if not __amounts_wrapper:
        __amounts_wrapper = AmountsWrapper(redis())
    return __amounts_wrapper
