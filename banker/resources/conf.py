PORT = 5000
DEBUG = True
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_CONCAT_SYMBOL = '+'

# Banker will request this amount money from Redis when needed.
AMOUNT_CHUNK = 20
# When a local campaign balance is less than this value, ask for more.
MIN_AMOUNT_TO_REQUEST_MORE = 3


